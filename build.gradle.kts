// Top-level build file where you can add configuration options common to all sub-projects/modules.
apply(plugin = "io.gitlab.arturbosch.detekt")
apply(plugin = "checkstyle")

buildscript {

    repositories {
        google() // Google's Maven repository
        jcenter()
        mavenCentral()
        gradlePluginPortal()
    }
    dependencies {
        classpath ("androidx.navigation:navigation-safe-args-gradle-plugin:2.3.3")
        //classpath ("androidx.navigation:navigation-safe-args-gradle-plugin:${ext["navigationVersion"]}")
        classpath ("com.google.gms:google-services:${Versions.googleServices}")
        classpath ("com.android.tools.build:gradle:${Versions.gradle}")
        classpath ("org.jetbrains.kotlin:kotlin-gradle-plugin:1.4.21")
        classpath ("org.jacoco:org.jacoco.core:${Versions.jacoco}")
        //classpath ("org.jetbrains.kotlin:kotlin-gradle-plugin:${ext["kotlinVersion"]}")
        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files

        classpath("io.gitlab.arturbosch.detekt:detekt-gradle-plugin:1.18.1")
    }
}

allprojects {
    repositories {
        mavenCentral()
        jcenter()
        google()
        maven (url  = "https://dl.google.com/dl/android/maven2")

        /*  maven {
              url "https://jitpack.io"
          }*/
    }

    tasks {

        register("checkstyle") {
        }

        // True in case there are Roboelectric tests in the app
        withType<Test> {
            //useJUnitPlatform()

            reports {
                junitXml.isEnabled = true
                html.isEnabled = false
            }
        }

        withType<Checkstyle>().configureEach {
            reports {
                xml.required.set(false)
                html.required.set(true)
                html.stylesheet = resources.text.fromFile("config/xsl/checkstyle-custom.xsl")
            }
        }

        /* withType<io.gitlab.arturbosch.detekt.Detekt> {
             // Target version of the generated JVM bytecode. It is used for type resolution.
             this.jvmTarget = "1.8"
         }*/

    }

}

tasks {
    register("clean", Delete::class) {
        delete (rootProject.buildDir)
    }
}

