/*
import org.gradle.kotlin.dsl.`kotlin-dsl`
*/

plugins {
    /**
     * [Kotlin DSL Plugin]
     * (https://docs.gradle.org/current/userguide/kotlin_dsl.html#sec:kotlin-dsl_plugin)
     */
    `kotlin-dsl`
    id("io.gitlab.arturbosch.detekt") version "1.17.1"
}

repositories {
    google()
    jcenter()
    mavenCentral()
    gradlePluginPortal()
}

// Creates a task for each module
/*subprojects{
    detekt {
        //config = rootProject.files("config/detekt/detekt.yml")
        buildUponDefaultConfig = true // preconfigure defaults
        // Android: Don't create tasks for the specified build flavor (e.g. "production")
        // ignoredFlavors = listOf("production")
        reports {
            html.enabled = true
            xml.enabled = true // checkstyle like format mainly for integrations like Jenkins
            txt.enabled = true // similar to the console output, contains issue signature to manually edit baseline files
            sarif.enabled = false // standardized SARIF format (https://sarifweb.azurewebsites.net/) to support integrations with Github Code Scanning
        }
    }
}*/

kotlinDslPluginOptions {
    experimentalWarning.set(false)
}

dependencies {
    implementation("com.android.tools.build:gradle:4.0.1")
    implementation("org.jetbrains.kotlin:kotlin-gradle-plugin:1.4.21")
    implementation(gradleApi())
    implementation(localGroovy())
}
